import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'Parcial_Practico',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
