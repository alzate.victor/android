// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyAeEK7EDi1k0NdALXgAZG8j_wbCjKz8Ums',
    authDomain: 'parcial-40ee5.firebaseapp.com',
    projectId: 'parcial-40ee5',
    storageBucket: 'parcial-40ee5.appspot.com',
    messagingSenderId: '76570476562',
    appId: '1:76570476562:web:f56af94e84305c36b758cb',
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
