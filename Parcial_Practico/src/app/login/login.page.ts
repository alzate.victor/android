import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { UserService } from './../services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  postData = { username: '', password: '' };

  constructor(
    private router: Router,
    private alertController: AlertController,
    private userService: UserService
  ) {}

  validateInputs() {
    const username = this.postData.username.trim();
    const password = this.postData.password.trim();
    return (
      this.postData.username &&
      this.postData.password &&
      username.length > 0 &&
      password.length > 0
    );
  }

  ngOnInit() {}

  login() {
    console.log(this.postData);
    // validar los datos de entrada
    if (this.validateInputs()) {
      this.userService.findAll().subscribe(
        (res: any) => {
          console.log(res);
          const { username, password } = this.postData;
          const index = res.findIndex((item: any) => {
            return item.username === username && item.password === password;
          });
          if (index >= 0) {
            // redireccionar al home
            this.router.navigate(['tabs']);
          } else {
            console.log('Incorrect username and password.');
            this.presentAlert('Incorrect username and password.');
          }
        },
        (error: any) => {
          console.log('Network Issue.');
          this.presentAlert('Network Issue.');
        }
      );
    } else {
      console.log('Please enter email/username or password.');
      this.presentAlert('Please enter email/username or password.');
    }
  }

  async presentAlert(message: string) {
    const alert = await this.alertController.create({
      header: 'Alert',
      subHeader: 'Important message',
      message,
      buttons: ['OK'],
    });

    await alert.present();
  }
}
