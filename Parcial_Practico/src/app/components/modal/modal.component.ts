import { Component, Input, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';
import { ModalController, ToastController } from '@ionic/angular';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
})
export class ModalComponent implements OnInit {
  @Input() id: string = '';
  note: any = null;

  constructor(
    private dataService: DataService,
    private modalController: ModalController,
    private toastController: ToastController
  ) {}

  ngOnInit() {
    this.dataService.getNoteById(this.id).subscribe((res) => {
      this.note = res;
    });
  }

  async deleteNote() {
    await this.dataService.deleteNote(this.note);
    this.modalController.dismiss();
  }

  async updateNote() {
    await this.dataService.updateNote(this.note);
    const toast = await this.toastController.create({
      message: 'Note updated!.',
      duration: 2000,
    });
    toast.present();
  }
}
