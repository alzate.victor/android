import { Component, OnInit } from '@angular/core';
import { AlertController, SearchbarCustomEvent } from '@ionic/angular';
import { NoteService } from './../services/note.service';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss'],
})
export class Tab1Page implements OnInit {
  notes: any = [];
  filtered: any = [];

  constructor(
    private noteService: NoteService,
    private alertController: AlertController
  ) {}

  ngOnInit() {
    this.getNotes();
  }

  getNotes() {
    this.noteService.findAll().subscribe(
      (res: any) => {
        console.log(res);
        this.notes = res;
        this.filtered = this.notes;
      },
      (error: any) => {
        console.log('Network Issue.');
        this.presentAlert('Network Issue.');
      }
    );
  }

  async presentAlert(message: string) {
    const alert = await this.alertController.create({
      header: 'Alert',
      subHeader: 'Important message',
      message,
      buttons: ['OK'],
    });

    await alert.present();
  }

  searchTerm(event: SearchbarCustomEvent) {
    const filter = event.detail.value?.toLowerCase();
    this.filtered = this.notes.filter((note: any) => {
      return note.title.toLowerCase().indexOf(filter) >= 0;
    });
  }
}
