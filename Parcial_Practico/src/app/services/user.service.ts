import { Injectable } from '@angular/core';
// HttpClient
import { HttpClient, HttpHeaders } from '@angular/common/http';
// rxjs
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  apiUrl: string =
    'https://run.mocky.io/v3/d87287e7-f865-4cc8-89e1-0c6a7f52482e';
  constructor(private http: HttpClient) {}

  public findAll(query = {}): Observable<any> {
    return this.http.get<any>(this.apiUrl);
  }
}
