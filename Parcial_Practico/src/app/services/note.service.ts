import { Injectable } from '@angular/core';
// HttpClient
import { HttpClient, HttpHeaders } from '@angular/common/http';
// rxjs
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class NoteService {
  apiUrl: string = 'https://jsonplaceholder.typicode.com/todos';
  notesUrl = 'api/notes';

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  };

  constructor(private http: HttpClient) {}

  public findAll(query = {}): Observable<any> {
    return this.http.get<any>(this.apiUrl);
  }

  public getNotes(query = {}): Observable<any> {
    return this.http.get<any>(this.notesUrl);
  }
}
