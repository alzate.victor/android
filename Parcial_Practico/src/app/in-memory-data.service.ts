import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';

@Injectable({
  providedIn: 'root',
})
export class InMemoryDataService implements InMemoryDbService {
  constructor() {}

  createDb() {
    const notes = [
      { id: 1, title: 'message 1' },
      { id: 2, title: 'message 2' },
    ];
    return { notes };
  }
}
