import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { AlertController, ModalController } from '@ionic/angular';
import { DataService } from './../services/data.service';
import { ModalComponent } from './../components/modal/modal.component';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss'],
})
export class Tab2Page implements OnInit {
  notes: any = [];

  constructor(
    private dataService: DataService,
    private alertController: AlertController,
    private modalController: ModalController
  ) {}

  ngOnInit() {
    this.getNotes();
  }

  getNotes() {
    this.dataService.getNotes().subscribe(
      (res: any) => {
        console.log(res);
        this.notes = res;
      },
      (error: any) => {
        console.log('Network Issue.');
        this.presentAlert('Network Issue.');
      }
    );
  }

  async addNote() {
    const alert = await this.alertController.create({
      header: 'Add Note',
      inputs: [
        {
          name: 'title',
          placeholder: 'Enter title...',
          type: 'text',
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
        },
        {
          text: 'Add',
          handler: (res) => {
            this.dataService.addNote({ title: res.title });
          },
        },
      ],
    });

    await alert.present();
  }

  async presentAlert(message: string) {
    const alert = await this.alertController.create({
      header: 'Alert',
      subHeader: 'Important message',
      message,
      buttons: ['OK'],
    });

    await alert.present();
  }

  async openNote(note: any) {
    const modal = await this.modalController.create({
      component: ModalComponent,
      componentProps: { id: note.id },
      breakpoints: [0, 0.5, 0.8],
      initialBreakpoint: 0.8,
    });

    await modal.present();
  }
}
