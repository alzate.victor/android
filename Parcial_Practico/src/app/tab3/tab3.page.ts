import { Component, OnInit } from '@angular/core';
// import { Network } from '@awesome-cordova-plugins/network/ngx';
// import { Network } from '@ionic-native/network/ngx';
// import { Dialogs } from '@ionic-native/dialogs/ngx';
import { Network } from '@capacitor/network';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss'],
})
export class Tab3Page implements OnInit {
  /*
  constructor(private network: Network) {
    // watch network for a disconnection
    let disconnectSubscription = this.network.onDisconnect().subscribe(() => {
      console.log('network was disconnected :-(');
    });

    // stop disconnect watch
    disconnectSubscription.unsubscribe();

    // watch network for a connection
    let connectSubscription = this.network.onConnect().subscribe(() => {
      console.log('network connected!');
      // We just got a connection but we need to wait briefly
      // before we determine the connection type. Might need to wait.
      // prior to doing any api requests as well.
      setTimeout(() => {
        if (this.network.type === 'wifi') {
          console.log('we got a wifi connection, woohoo!');
        }
      }, 3000);
    });

    // stop connect watch
    connectSubscription.unsubscribe();
  }
  */

  /*
  constructor(private network: Network, private dialogs: Dialogs) {
    this.network.onDisconnect().subscribe(() => {
      console.log('network was disconnected :-(');
      this.dialogs.alert('network was disconnected :-(');
    });

    this.network.onConnect().subscribe(() => {
      this.dialogs.alert('network connected!');
      setTimeout(() => {
        console.log('we got a wifi connection, woohoo!');
        this.dialogs.alert(
          'we got a ' + this.network.type + ' wifi connection, woohoo!'
        );
      }, 3000);
    });
  }
  */

  /*
  networkListener: PluginListenerHandle;
  status: any;

  constructor() {}

  async ngOnInit() {
    this.networkListener = await Network.addListener(
      'networkStatusChange',
      (status) => {
        console.log('Network status changed', status);
      }
    );
    const status = await Network.getStatus();
    console.log('Network status:', status);
    this.changeStatus(status);
    console.log('Network status:', this.status);
  }

  changeStatus(status: any) {
    this.status = status?.connected;
  }

  ngOnDestroy() {
    if (this.networkListener) this.networkListener.remove();
  }
  */

  status: boolean = true;

  constructor(private toastController: ToastController) {}

  ngOnInit(): void {
    Network.addListener('networkStatusChange', (status) => {
      console.log('Network status changed', status);
      if (status.connected) {
        console.log('Network connected');
        this.status = true;
      } else {
        console.log('Network disconnected');
        this.status = false;
      }
    });
    this.showToast(this.status ? 'You are online' : 'You are offline');
  }

  async showToast(message: string) {
    const toast = await this.toastController.create({
      message,
      duration: 2000,
    });
    toast.present();
  }
}
